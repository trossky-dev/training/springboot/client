import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRouting } from './app-routing.module';
import { AppComponent } from './app.component';
import { GreetingService } from './greeting.service';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AuthModule } from './auth/auth.module';
import { WelcomeModule } from './welcome/welcome.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRouting),
    HttpClientModule,
    AuthModule,
    WelcomeModule
  ],
  providers: [GreetingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
