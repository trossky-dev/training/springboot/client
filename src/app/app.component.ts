import { Component } from '@angular/core';
import { GreetingService } from './greeting.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'tky-test';
  greeting: any;
  constructor(private greetingSrvice: GreetingService) {

  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.

    this.greetingSrvice.greeting()
      .subscribe(data => {
        if (data) {
          this.greeting=data
        } else {
          alert("NOT FOUND")  
        }
      }, error => {
        alert("ERROR EN SERVIDOR")
      })

  }
}
