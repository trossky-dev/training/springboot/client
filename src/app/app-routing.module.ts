import { Routes } from '@angular/router';
import { AuthRouting } from './auth/auth-routing.module';
import { WelcomeModule } from './welcome/welcome.module';
import { WelcomeRoutingModule } from './welcome/welcome-routing.module';

export const AppRouting: Routes = [
  {path:'', redirectTo:'login', pathMatch:'full'},
  ...AuthRouting,
  ...WelcomeRoutingModule,
  { path: '*', redirectTo: '/login', pathMatch: 'full' }
];

