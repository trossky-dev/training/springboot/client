
import { Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';

export const WelcomeRoutingModule: Routes = [
  {path:'welcome', component:WelcomeComponent},
  { path: '*', redirectTo: '/login', pathMatch: 'full' }
];


