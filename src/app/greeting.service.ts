import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GreetingService {
  URL_API="http://localhost:8080"
  URI="/greeting"

  constructor( private http:HttpClient ) { }

  greeting(){
    return this.http.get<any>(this.URL_API+this.URI)
  }
}
